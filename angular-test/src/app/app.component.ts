import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  tiles: TilesType[] = [
    { title: 'Todo', count: 2 },
    { title: 'Alarm', count: 1 },
    { title: 'History', count: 0 },
  ];

  newTodo: Note;
  newAlarm: Note;
  newHistory: Note;

  addANote(formValues) {
    const note = {
      desc: formValues.desc,
      date: formValues.date,
      type: formValues.type,
    };
    if (formValues.type === 'Todo') {
      this.newTodo = note;
    } else if (formValues.type === 'Alarm') {
      this.newAlarm = note;
    }
  }

  updateCount(typecount) {
    if (typecount.type === 'Alarm') {
      this.tiles[1].count = typecount.count;
    } else if (typecount.type === 'Todo') {
      this.tiles[0].count = typecount.count;
    } else if (typecount.type === 'History') {
      this.tiles[2].count = typecount.count;
    }
  }

  sendHistory(oldnote) {
    const note = { desc: oldnote.desc, date: oldnote.date, type: oldnote.type };
    this.newHistory = note;
  }
}

interface TilesType {
  title: string;
  count: number;
}

export interface Note {
  desc: string;
  date: number;
  type: string;
}
