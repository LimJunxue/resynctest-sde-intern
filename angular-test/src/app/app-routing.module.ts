import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlarmComponent } from './alarm/alarm.component';
import { HistoryComponent } from './history/history.component';
import { TodoComponent } from './todo/todo.component';

const routes: Routes = [
  { path: 'todoComponent', component: TodoComponent },
  { path: 'alarmComponent', component: AlarmComponent },
  { path: 'historyComponent', component: HistoryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
