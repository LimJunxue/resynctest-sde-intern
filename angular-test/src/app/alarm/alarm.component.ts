import {
  Component,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  EventEmitter,
} from '@angular/core';
import { Note } from '../app.component';

@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.component.html',
  styleUrls: ['./alarm.component.scss'],
})
export class AlarmComponent implements OnInit, OnChanges {
  type = 'Alarm';
  @Input() newAlarm: Note;
  @Output() count = new EventEmitter<object>(true);
  @Output() history = new EventEmitter<Note>(true); // async events in the case of multiple notes sent to history
  laterDateTime = new Date().setMinutes(new Date().getMinutes() + 1, 0, 0);

  alarms: Note[] = [
    // dummy values
    {
      desc: 'alarm 1',
      date: this.laterDateTime,
      type: this.type,
    },
  ];

  constructor() {}

  ngOnInit() {
    this.everyMinuteOnTheMinute();
  }

  // adjust timeout every minute and calls checkAlarmsTime function
  everyMinuteOnTheMinute() {
    setTimeout(() => {
      this.checkAlarmsTime();
      this.everyMinuteOnTheMinute();
    }, 60000 - (Date.now() % 60000));
  }

  checkAlarmsTime() {
    const indicesToBeDeleted: number[] = [];
    this.alarms.forEach((alarm, index) => {
      if (alarm.date === new Date().setSeconds(0, 0)) {
        alert('Alarm: ' + alarm.desc);
        this.history.emit(alarm);
        indicesToBeDeleted.push(index);
      } else if (alarm.date <= new Date().setSeconds(0, 0)) {
        this.history.emit(alarm);
        indicesToBeDeleted.push(index);
      }
    });
    this.massDelete(indicesToBeDeleted);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.newAlarm === undefined) {
      return;
    }

    // check alarm time
    if (this.newAlarm.date <= new Date().setSeconds(0, 0)) {
      alert('You just set an alarm for the past:\n' + this.newAlarm.desc);
      // don't add this alarm into the list
    } else {
      // add newAlarm from updated app into list
      const count = this.alarms.push({
        desc: this.newAlarm.desc,
        date: this.newAlarm.date,
        type: this.type,
      });
    }
    if (this.alarms.length > 0) {
      this.alarms = this.alarms.sort((a, b) => a.date - b.date); // sorts todos with earliest date on top
    }
    this.count.emit({ type: this.type, count: this.alarms.length }); // update count in app tiles
  }

  massDelete(arr: number[]) {
    // iterate backwards to prevent messing up order
    for (let i = arr.length - 1; i >= 0; i--) {
      this.alarms.splice(arr[i], 1);
    }
    this.count.emit({ type: this.type, count: this.alarms.length }); // update count in app tiles
  }

  delete(index) {
    const deletedElement = this.alarms.splice(index, 1);
    this.history.emit(deletedElement[0]);
    this.count.emit({ type: this.type, count: this.alarms.length }); // update count in app tiles
  }

  editAlarm(newValues, index) {
    this.alarms[index].date = newValues.date;
    this.alarms[index].desc = newValues.desc;
  }
}
