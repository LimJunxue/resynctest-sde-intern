import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Note } from '../app.component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit, OnChanges {
  type = 'History';
  history: Note[] = [];
  @Input() newNote: Note;
  @Output() count = new EventEmitter<object>(true); // async events in the case of multiple notes sent to history

  constructor() {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.newNote === undefined) {
      return;
    }

    const count = this.history.push({
      desc: this.newNote.desc,
      date: this.newNote.date,
      type: this.newNote.type,
    });
    if (this.history.length > 0) {
      this.history = this.history.sort((a, b) => a.date - b.date); // sorts history with earliest date on top
    }
    this.count.emit({ type: this.type, count });
  }

  delete(index) {
    this.history.splice(index, 1);
    this.count.emit({ type: this.type, count: this.history.length }); // update count in app tiles
  }
}
