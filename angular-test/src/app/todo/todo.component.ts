import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Note } from '../app.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
})
export class TodoComponent implements OnInit, OnChanges {
  type = 'Todo';
  @Input() newTodo: Note;
  @Output() count = new EventEmitter<object>(true);
  @Output() history = new EventEmitter<Note>(true); // async events in the case of multiple notes sent to history

  todos: Note[] = [
    // dummy values
    {
      desc: 'do 1',
      date: new Date().setSeconds(0, 0),
      type: this.type,
    },
    {
      desc: 'do 2',
      date: new Date().setSeconds(0, 0),
      type: this.type,
    },
  ];

  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.newTodo === undefined) {
      return;
    }

    // check todo time
    if (this.newTodo.date <= new Date().setSeconds(0, 0)) {
      alert('You just set a todo for the past:\n' + this.newTodo.desc);
      // don't add this todo into the list
    } else {
      // add newTodo from updated app to list
      const count = this.todos.push({
        desc: this.newTodo.desc,
        date: this.newTodo.date,
        type: this.type,
      });
    }
    if (this.todos.length > 0) {
      this.todos = this.todos.sort((a, b) => a.date - b.date); // sorts todos with earliest date on top
    }
    this.count.emit({ type: this.type, count: this.todos.length }); // update count in app tiles
  }

  ngOnInit() {
    this.everyMinuteOnTheMinute();
  }

  // adjust timeout every minute and calls checkTodosTime function
  everyMinuteOnTheMinute() {
    setTimeout(() => {
      this.checkTodosTime();
      this.everyMinuteOnTheMinute();
    }, 60000 - (Date.now() % 60000));
  }

  checkTodosTime() {
    const indicesToBeDeleted: number[] = [];
    this.todos.forEach((todo, index) => {
      if (todo.date <= new Date().setSeconds(0, 0)) {
        this.history.emit(todo);
        indicesToBeDeleted.push(index);
      }
    });
    this.massDelete(indicesToBeDeleted);
  }

  massDelete(arr: number[]) {
    // iterate backwards to prevent messing up order
    for (let i = arr.length - 1; i >= 0; i--) {
      this.todos.splice(arr[i], 1);
    }
    this.count.emit({ type: this.type, count: this.todos.length }); // update count in app tiles
  }

  delete(index) {
    const deletedElement = this.todos.splice(index, 1);
    this.history.emit(deletedElement[0]);
    this.count.emit({ type: this.type, count: this.todos.length }); // update count in app tiles
  }

  editTodo(newValues, index) {
    this.todos[index].date = newValues.date;
    this.todos[index].desc = newValues.desc;
  }
}
