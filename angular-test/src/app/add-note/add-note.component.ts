import {
  Component,
  OnInit,
  Output,
  TemplateRef,
  EventEmitter,
  Injectable,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ModalDismissReasons,
  NgbModal,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class ModalState {
  modalRef: NgbModalRef;
  template: TemplateRef<any>;
}

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
})
export class AddNoteComponent implements OnInit {
  closeResult: string;
  minDate = undefined;
  form: FormGroup;
  // modalRef: NgbModalRef;
  @Output() add = new EventEmitter(false);

  constructor(
    private modalState: ModalState,
    private modalService: NgbModal,
    private formBuilder: FormBuilder
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate(),
    };
    this.form = this.formBuilder.group({
      desc: [null, Validators.required],
      date: [null, Validators.required],
      time: [null, Validators.required],
      type: ['Todo', Validators.required],
    });
    this.form.patchValue({ type: 'Todo' });
  }

  ngOnInit() {}

  open(content) {
    this.modalState.template = content;
    this.modalState.modalRef = this.modalService.open(this.modalState.template);
    this.modalState.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  submit() {
    console.log(this.form.value);
    const ngbDate = this.form.value.date;
    const time = this.form.value.time.split(':');
    const hour = time[0];
    const minute = time[1];
    // send note to app to add to the appropriate tile
    this.add.emit({
      desc: this.form.value.desc,
      date: new Date(
        ngbDate.year,
        ngbDate.month - 1,
        ngbDate.day,
        hour,
        minute
      ).getTime(),
      type: this.form.value.type,
    });
    this.modalState.modalRef.close();
    this.form.reset();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
