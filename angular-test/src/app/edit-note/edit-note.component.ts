import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  ModalDismissReasons,
  NgbDate,
  NgbModal,
  NgbModalRef,
} from '@ng-bootstrap/ng-bootstrap';
import { Note } from '../app.component';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.scss'],
})
export class EditNoteComponent implements OnInit {
  form: FormGroup;
  modalRef: NgbModalRef;
  closeResult: string;
  minDate = undefined;
  @Input() editingNote: Note;
  @Output() editTodo = new EventEmitter(false);
  @Output() editAlarm = new EventEmitter(false);

  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate(),
    };
    this.form = this.formBuilder.group({
      desc: [null, Validators.required],
      date: [null, Validators.required],
      time: [null, Validators.required],
    });
  }

  ngOnInit() {}

  open(content: TemplateRef<any>) {
    const date = new Date(this.editingNote.date);
    const ngbDate = new NgbDate(
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate()
    );
    this.form.setValue({
      desc: this.editingNote.desc,
      date: ngbDate,
      time: date.getHours() + ':' + date.getMinutes(),
    });
    this.modalRef = this.modalService.open(content);
    this.modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
      },
      (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  submit() {
    console.log(this.form.value);
    const ngbDate = this.form.value.date;
    const time = this.form.value.time.split(':');
    const hour = time[0];
    const minute = time[1];
    const updatedNote = {
      desc: this.form.value.desc,
      date: new Date(
        ngbDate.year,
        ngbDate.month - 1,
        ngbDate.day,
        hour,
        minute
      ).getTime(),
    };
    // send note to the appropriate component
    if (this.editingNote.type === 'Todo') {
      this.editTodo.emit(updatedNote);
    } else if (this.editingNote.type === 'Alarm') {
      this.editAlarm.emit(updatedNote);
    }
    this.modalRef.close();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
