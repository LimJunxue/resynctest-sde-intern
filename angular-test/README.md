# NoteApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.

1. Note Tab: User can Add/Edit/Delete notes like, Text, bullet points.
   When user save/update timestamp should be updated and based on that Older notes from current time should be display in history tab and remove
   note tab.
   For example, A note was created yesterday, it will show up in history tab. If I update it now, the timestamp will updated and the older note would be changed/removed from the history tab and replaced with the new note.

2. Alarm Tab: User can Add/Edit/Delete Alarm/reminder with Date and time (Future time only). When the configured time comes application should notify user
   by notification/banner pop-up. Alarms/reminders which are older than current time should be display in history tab and remove from alarm tab.
3. History Tab: History tab only shows past notes and alarms. User can only delete in history tab. Cannot Add new or Edit existing.
4. If you have taken any assumptions while creating the application, just specify that in your repo and email to us.

### Notes from editor @LimJunxue

- This version was done in 2.5 days from 5th to 7th Mar, because I had to study for midterm tests on the 4th and 5th.
- This was also done with no prior knowledge in Angular, so please forgive my software architecture design.
- An assumption made was that notes are todos, and I gave notes a deadline field, since all 'notes' are created in the past and by the instructions above, 'notes' will always immediately go to the history.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
